import datetime
import pandas
from boto3.session import Session
import time

if __name__ == '__main__':
    # ---------------------------------------------------------------------
    # Generate position data and Send position data to Kinesis data stream
    # ---------------------------------------------------------------------

    # Modify this section to reflect your AWS configuration.
    awsRegion = 'ap-northeast-1'         # The AWS region where your Kinesis Analytics application is configured.
    accessKeyId = 'AKIARIINAMJ5UTQ4R3II'       # Your AWS Access Key ID
    secretAccessKey = 'RuD03h7yWC0x7Mxh5hkc0ySCyhHtIZh4po3h4hsQ'   # Your AWS Secret Access Key
    inputStream = 'kinesisdatastream-YAMAKI'       # The name of the stream being used as input into the Kinesis Analytics hotspots application

    session = Session(aws_access_key_id=accessKeyId,
                      aws_secret_access_key=secretAccessKey,
                      region_name='ap-northeast-1')

    kinesis = session.client('kinesis')
    
    # ---------------------------------------------------------------------
    # Put 100 records/sec to Kinesis data stream for 60 sec, 1 record/sec has 100 user posidata
    # ---------------------------------------------------------------------
    period = 60
    
    records_all = pandas.read_pickle("./data/60sec_records_facility100_user50.pickle").values.tolist()   
    put_timestamp_list = []
    for k in range(period):
        records = records_all[k]
        kinesis.put_records(StreamName=inputStream, Records=records)
        put_timestamp = datetime.datetime.now()
        put_timestamp_list.append(put_timestamp)
        time.sleep(1.000000)
        
    file_name = str(datetime.datetime.now()).replace(" ", "_").replace(":", "").replace("-", "").replace(".", "_")
    pandas.DataFrame(put_timestamp_list).to_csv("./put_timestamp_result/{}.csv".format(file_name))
